# eslint-config-pollinate

## Purpose

This is a node package that houses [ESLint](http://eslint.org/) configurations. It enforces a basic [standard](https://youtu.be/I9cP-1kC3So?t=1m47s) agreed upon by some of the maintainers of Pollinate’s core JavaScript libraries.

If you’re the maintainer of a project using these standards and wish to change or override these rules, you can:

* Open a pull request to change them for everyone.
* Take advantage of ESLint’s [cascading and hierarchy model](http://eslint.org/docs/user-guide/configuring#configuration-cascading-and-hierarchy) to override them for a specific directory.
* [Use comment-based configurations](http://eslint.org/docs/2.0.0/user-guide/configuring) for a specific file or even lines within the file.

The principle behind these standards is deviations from these standards are OK, but they should be a conscious decision, not a casual oversight.

## `console` and `debugger`

`console` and `debugger` statements are allowed with a warning to keep debugging easy. However, this convenience comes at a cost.

`debugger` statements will stop JavaScript execution and should _never_ be allowed in production. As such, you should consider how you will prevent them from making their way into production. One possible arrangement is to trigger a build failure on ESLint warnings for anything other than local development.

Verbose, always-on logging can have adverse performance effects in node and clutters shared environments like a browser where more than one script is likely to be running. Ideally, your app or library should use a logging module that’s set up with an inline ESLint rule and toggled on or off depending on an environment variable or query string parameter.

## Installation

To install in your project:

```shell
$ npm install --save-dev eslint eslint-config-pollinate
```

You may have multiple environments (e.g. node build scripts and browser code) in the same repo. You can set up multiple `.eslintrc.json` files in the directories of each of your environments:

**`.eslintrc.json` for generic browser code**

```json
{
  "extends": [
    "pollinate/environments/browser"
  ]
}
```

**`.eslintrc.json` for Vue code** (requires [eslint-plugin-vue](https://www.npmjs.com/package/eslint-plugin-vue) and [babel-eslint](https://www.npmjs.com/package/babel-eslint))

```json
{
  "extends": [
    "pollinate/environments/vue"
  ]
}
```

**`.eslintrc.json` for node code**

```json
{
  "extends": [
    "pollinate/environments/node"
  ]
}
```

**`.eslintrc.json` for universal (a.k.a. isomorphic) code**

```json
{
  "extends": [
    "pollinate/environments/universal"
  ]
}
```

You may also want to add [environments for common libraries](http://eslint.org/docs/user-guide/configuring#specifying-environments) and an ESLint plugin for templating languages such as JSX that mix JavaScript with proprietary markup.

**Last but not least: set your build up to enforce ESLint.** JavaScript developers tend to use a variety of editors set up in a variety of ways. It’s up to you to ensure that a contributor to your project who *isn’t* running ESLint in *their* editor won’t run afoul of your project’s rules and trip up the next contributor who *is* running ESLint in *theirs*. ESLint has plugins [for grunt](https://www.npmjs.com/package/grunt-eslint), [gulp](https://github.com/adametry/gulp-eslint) and [webpack](https://github.com/MoOx/eslint-loader). If you’re rolling your own build from scratch, ESLint’s CLI is pretty straightforward and is [also accessible via a JavaScript API](http://eslint.org/docs/developer-guide/nodejs-api#cliengine).

## Automatic fixing

If you want linting errors to be automatically corrected, you have a couple of options:

1. Set your build up to run [the `--fix` option in the ESLint cli](http://eslint.org/docs/user-guide/command-line-interface#options).
2. Use a tool like [`eslint-to-editorconfig`](https://www.npmjs.com/package/eslint-to-editorconfig) to dynamically generate an `.editorconfig` file.

There is no straightforward way to incorporate either of these options into this package, so they’re both optional and up to you to implement.

## Versioning

This package follows [semantic versioning](http://semver.org/) (`major.minor.patch`). In this case:

* A **patch** change fixes a bug or removes a rule. This should not generate any new linting errors.
* A **minor** change may add a new rules that may generate some new linting errors, but they should be easily fixed.
* A **major** change adds significant new rules, renames or restructures existing rules, or upgrades the ESLint dependency to a new major version.
